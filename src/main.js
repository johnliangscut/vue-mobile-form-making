import Vue from 'vue'
import App from './App.vue'
import Toast from './components/toast/index'
import './common/register'
import './style/global.less'

Vue.use(Toast)

Vue.config.productionTip = false

new Vue({
  data: {
    EventBus: new Vue()
  },
  render: h => h(App),
}).$mount('#app')
