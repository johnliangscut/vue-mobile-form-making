import KwTabs from './KwTabs'
import KwTabPane from './KwTabPane'
import KwAccordion from './KwAccordion'
import KwAccordionItem from './KwAccordionItem'
import KwDialog from './KwDialog'
import KwTree from './KwTree'
import KwCheckbox from './KwCheckbox'

export default {
  KwTabs,
  KwTabPane,
  KwAccordion,
  KwAccordionItem,
  KwDialog,
  KwTree,
  KwCheckbox
}