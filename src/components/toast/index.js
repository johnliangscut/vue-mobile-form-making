import Toast from './Toast'
let _TOAST = {
    show: false,
    component: null
}

export default {
    install(Vue) {
        Vue.prototype.$toast = function(text, opts) {
            let defaultOpts = {
                position: 'center',
                duration: 1500,
                wordWrap: false
            };
            opts = Object.assign(defaultOpts, opts);
            let wordWrap = opts.wordWrap? 'kw-word-wrap': '',
                style = opts.width? `style="width: ${opts.width}"` : ''
            if (_TOAST.show) {
                return
            }
            if (!_TOAST.component) {
                let ToastComponent = Vue.extend(Toast);
                _TOAST.component = new ToastComponent()
                _TOAST.component.$mount()
                document.body.appendChild(_TOAST.component.$el)
            }
            _TOAST.component.wordWrap = wordWrap
            _TOAST.component.style = style
            _TOAST.component.position = `kw-toast-${opts.position}`
            _TOAST.component.text = text
            _TOAST.component.show = _TOAST.show = true
            setTimeout(function() {
                _TOAST.component.show = _TOAST.show = false
            }, opts.duration)
        }
    }
}