import inputProp from './inputProp'
import textareaProp from './textareaProp'
import selectProp from './selectProp'
import optionsProp from './optionsProp'
import checkboxProp from './checkboxProp'

export default {
    inputProp,
    textareaProp,
    selectProp,
    optionsProp,
    checkboxProp
}