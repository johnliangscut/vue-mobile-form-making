import { CHANGE_COMPONENT_EVENT } from '../common/event'

export const prop = {
  props: {
    setting: {
      default() {
        return {}
      }
    },
    config: {
      default() {
        return {
          prop: 'label',
          maxLength: 10
        }
      }
    }
  },
  data() {
    return {
      defaultValue: this.setting[this.config.prop] || ''
    }
  },
  watch: {
    setting(val) {
      this.setting = val
      this.defaultValue = val[this.config.prop]
    }
  },
  methods: {
    handlerPropChange() {
      let obj = Object.assign(this.setting)
      obj[this.config.prop] = this.defaultValue
      this.$root.EventBus.$emit(CHANGE_COMPONENT_EVENT, obj)
    }
  }
}