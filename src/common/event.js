export const MOVE_START_EVENT = 'moveStart'
export const MOVE_EVENT = 'move'
export const MOVE_END_EVENT = 'moveEnd'
export const MOVE_IN_CANVAS_EVENT = 'moveInCanvas'
export const SELECT_COMPONENT_EVENT = 'selectComponent'
export const CHANGE_COMPONENT_EVENT = 'changeComponent'
export const CHANGE_INFO_EVENT = 'changeInfo'
export const DRAG_END_EVENT = 'dragend'
export const SAVE_EVENT = 'save'
export const CHANGE_TAB_EVENT = 'changeTab'
export const SCROLL_EVENT = 'scroll'
export const JSON_IMPORT_EVENT = 'importJson'
export const JSON_EXPORT_EVENT = 'exportJson'
export const JSON_DOWNLOAD_EVENT = 'downloadJson'