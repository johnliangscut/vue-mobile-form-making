export const generateCID =() => {
  return 'cid_' + new Date().getTime()
}

export const queryDomByAttr = (parentNode, value, key) => {
  key = key || 'cid'
  return parentNode.querySelector('*[' + key + '=' + value + ']')
}

export const getComponentIndexByCID =(components, cid) => {
  let index = -1
  if (components) {
    components.every((o, i) => {
      if (o.cid === cid) {
        index = i
      }
      return index === -1
    })
  }
  return index
}