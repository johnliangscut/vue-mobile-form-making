import Vue from 'vue'
import request from './request'
import cps from '../components/index'
import wgs from '../widgets/index'
import pops from '../props/index'

let comps = Object.assign(cps, wgs, pops)
Object.keys(comps).forEach((key) => {
  let item = cps[key]
  var cp_name=key.replace(/([A-Z])/g,"-$1").toLowerCase()
  if (cp_name && cp_name[0] === '-') {
    cp_name = cp_name.replace('-','')
  }
  Vue.component(cp_name, item)
})

Vue.use({
  install: (Vue) => {
    Object.keys(request).map(k => {
      Vue.prototype['$' + k] = request[k]
    })
  }
})