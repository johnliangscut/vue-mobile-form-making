# vue-mobile-form-making

## 参照网上开源的ddvue项目对代码来做修改。
https://github.com/wxjaa/ddvue / https://gitee.com/sanshu/ddvue
```
为致敬原作者，保留了页面背景及布局风格，以便便于识别最初出处
工程基于vue-cli4来创建
将源码内用ifelse语句写死的组件重新抽取封装为独立组件，可通过修改配置直接添加新的组件及属性
```

## PC端设计器地址
https://gitee.com/maqzh/vue-pc-form-making

## demo地址
http://form.kingwn.com/mmaking/index.html

## 效果图
![image text](https://gitee.com/maqzh/vue-mobile-form-making/raw/master/demo.jpg)

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
